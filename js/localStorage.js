//global variable
var search = window.location.search;
var album_id = getAlbumId(search);
window.onload = function(){
	if(album_id)
		loadPreviousState(album_id);
};

//----------------------------------SAVE DATA, AND THUMBNAIL IN LOCALSTORAGE---------------------------------------------------------------------------------------------
saveCurrentState = function(){
	createThumbnail(123, 100); //after creating the thumbnail, i call the 'storeData' in order to obtain sequentiality
}



function storeData(){
	var album_title = $('#title span').text();
	//i need to know the index of li element
	var background = -1;
	for(var index=1;index<4; index++){
		if ($('#background_selection li:nth-child('+index+')').children().is('.selected')){
			background = index-1;	
			break;
		}
	}	
	
	if(!album_id){
		album_id = parseInt(getMaxKey())+1; //autoincrement from the max value of the key
	}
	
	var imageList = [];
	var src,text,fill,font,x,y,rotation,width,height,shadowX,shadowY,shadowColor,border,thumbnail;
	var embossEffect, noiseEffect, sepiaEffect, invertEffect, greyEffect //valid only for the image
	
	if(workspace.children.length>0){
		for(var i=0; i<workspace.children.length; i++){
			var current_item = workspace.children[i];
			if(current_item.children[0].text){ //text
				src = "";
				text = current_item.children[0].text;
				fill = current_item.children[0].fill;
				font = current_item.children[0].font;
				//the shadowInformation is in the blur, differently from image
				if(current_item.children[0].shadow.blur == 0)
					shadowColor = 'transparent';
				else
					shadowColor = current_item.children[0].shadow.color;	
			}else{	//image
				src = current_item.children[0].image.src;
				text= "";
				fill = "";
				font = "";
				shadowColor = current_item.children[0].shadowColor;	
			}
			
			//variable extracted from current_item on workspace
			x = current_item.x ;
			y = current_item.y;
			rotation = current_item.rotation;
			width = current_item.width;
			height = current_item.height;
			shadowX = current_item.shadowX
			shadowY = current_item.shadowY;
			border = current_item.children[0].stroke;
			embossEffect = current_item.embossEffect;
			noiseEffect = current_item.noiseEffect;
			invertEffect = current_item.invertEffect;
			greyEffect = current_item.greyEffect;
			sepiaEffect = current_item.sepiaEffect;
			
			
			//json Object
			var itemJSON = {
				src: src,
				text: text,
				fill: fill, 
				font: font,
				x: x,
				y: y,
				rotation: rotation,
				width: width,
				height: height,
				shadowX: shadowX,
				shadowY: shadowY,
				shadowColor: shadowColor,
				border: border,
				embossEffect: embossEffect,
				noiseEffect: noiseEffect,
				invertEffect: invertEffect,
				greyEffect: greyEffect,
				sepiaEffect: sepiaEffect	
			};
			//append the item to the list
			imageList.push(itemJSON);
			
		}
	}
	//JSON object where there's the background and list of attributes
	var item = {
		title: album_title,
		background: background, //id of the background
		thumbnail: thumbnailDataURL, //thumbnail
		attributes: imageList, // attributes of the images//thumbnail
	};		
	
	//store the JSON in the local store, using album_name as key and array of elements as value
	localStorage.setItem(album_id, JSON.stringify(item));
}

//---------------------------------------------LOADING IMAGES AND TEXT, THE METHOD CALLS HIMSELF, SO I'M SURE THAT ARE SEQUENTIALLY----------------------------------------------
//global variables
var count=0;
var imageList = [];
var current_item; 
//ASYNCHRONOUS LOADING
loadPreviousState = function(album_id){
	var objectStored = JSON.parse(localStorage.getItem(album_id));
	$('#title span').text(objectStored.title);
	imageList = objectStored.attributes;	
	selectBackground(objectStored.background);
	drawItem();
}

//draw the attributes for the image
drawItem = function(){
	current_item = imageList[count];
	
	if(current_item.src != ""){
		insertImage(current_item.src, current_item.x, current_item.y,function(){
			//main attributes
			selectedImage.rotation = current_item.rotation;
			selectedImage.width = current_item.width;
			selectedImage.height = current_item.height;
			restoreImageScaling(); //restore the image size to the container size
			
			//effects
			selectedImage.embossEffect = current_item.embossEffect;
			selectedImage.noiseEffect = current_item.noiseEffect;
			selectedImage.invertEffect = current_item.invertEffect;
			selectedImage.greyEffect = current_item.greyEffect;
			selectedImage.sepiaEffect = current_item.sepiaEffect;
			
			//borders
			selectedImage.children[0].stroke = current_item.border;
			
			//it checks if we have to draw the shadow
			if(current_item.shadowColor != 'transparent'){
				selectedImage.shadowX = current_item.shadowX;
				selectedImage.shadowY = current_item.shadowY;
				shade(); 
			}
			
			unselect(selectedImage);
			
			count++;
			drawItem();				
		});
		
	}else{
		addCaption(current_item.text, current_item.fill, current_item.font, function(){
			selectedImage.x = current_item.x;
			selectedImage.y = current_item.y;
			selectedImage.rotation = current_item.rotation;
			selectedImage.children[0].height = current_item.height;
			selectedImage.children[0].width = current_item.width;
			selectedImage.height = current_item.height;
			selectedImage.width = current_item.width;
			
			//borders
			selectedImage.children[0].stroke = current_item.border;
				
			//it checks if we have to draw the shadow
			if(current_item.shadowColor != 'transparent'){
				selectedImage.shadowX = current_item.shadowX;
				selectedImage.shadowY = current_item.shadowY;
				shade(); 
			}
			
			count++;
			drawItem();			
		});	
	}
}

//------------------------------------------------------------HELPER METHODS---------------------------------------------------------------------------------------------------

//get the max value of the key
function getMaxKey(){
	var keys = Object.keys(localStorage);
	var max = 0;
	for(var i=0; i<localStorage.length; i++){
		if(!isNaN(parseInt(keys[i])))
			if(keys[i]>max)
				max = keys[i];
	}
	return max;
}

//regEx in order to extract the album_id
function getAlbumId(search){
	var album_id = search.match(/\d+/);
	return album_id;
}

//global thumbnail
var thumbnailDataURL;
//function to resize the canvas element
createThumbnail = function(width, height){
	//create a new canvas of the dimension of the thumbnail
	if(selectedImage)
		unselect(selectedImage);
		
	var canvas = document.getElementById("album_canvas");
	var thumbnail = new Image();
	
	
	thumbnail.onload = function(){
		 // Create a new canvas element, able to copy the current_canvas, scale and parse the image as dataURL
        canvas = document.createElement('canvas');
        canvas.setAttribute("width",width);
        canvas.setAttribute("height",height);
        //ratio for scaling
        var width_ratio = width/thumbnail.width;
        var height_ratio = height/thumbnail.height;
        //new context and draw image
        var context = canvas.getContext("2d");
		context.scale(width_ratio, height_ratio); 
		context.drawImage(thumbnail,0,0);
		thumbnailDataURL = canvas.toDataURL();
		//at the end of these operations, i call the method to store data in the DB 
		storeData(); 
	};
	thumbnail.src = canvas.toDataURL();
	
}

