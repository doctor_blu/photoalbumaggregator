$(document).ready(function() {
	document.getElementById('upload_background').addEventListener('change', handleFileSelect ,false);
});


function selectBackground(type) {
	//conversione between type and name
	var name = "";
	if (type == 0) name="modern";
	else if (type == 1) name="classic";
	else if (type == 2) displayBackground();
	
	// change the selected element in the ul
	$("#tools_left ul li a").removeClass("selected");
	$($("#tools_left ul li a")[type]).addClass("selected");
	
	//change the background image
	if (type<=2){
		//draw the image as background
		workspace.background.set("image(img/Background/"+name+".png)");	
	}	
};


//-----------------------------------------------------------------------------UPLOAD BACKGROUND MANAGER------------------------------------------------------------------------

//global imagaDataURL
var backgroundCroppedDataURL;
var imageToDataURL;
var width = 700;
var height = 600;

//function triggered when the upload starts
function handleFileSelect(evt){
	imageReader = new FileReader();
	var imageFile = evt.target.files.item(0);
	imageReader.readAsDataURL(imageFile);

	//onload function, when i've finished to load the file
	imageReader.onload = function (evt){
		imageToDataURL = evt.target.result;
		console.log(imageToDataURL);
		
		var backgroundCropped = new Image();
		backgroundCropped.src = imageToDataURL;
		backgroundCropped.onload = function(){
			canvas = document.createElement('canvas');
			canvas.setAttribute("width",width);
			canvas.setAttribute("height", height);
			ratio_width = width/backgroundCropped.width;
			ratio_height = height/backgroundCropped.height;
			//new context to draw the image
			var context = canvas.getContext("2d");
			context.scale(ratio_width, ratio_height);
			context.drawImage(backgroundCropped,0,0);
			console.log(context);
			//toDataURL
			backgroundCroppedDataURL = canvas.toDataURL();	
			try{
				if(backgroundCroppedDataURL)	
					localStorage.setItem("background", JSON.stringify(backgroundCroppedDataURL));
			}catch(e){
				console.log("storage failed: "+ e);
			}
			selectBackground(2);
		};	
	}	
};





//----------------------------------load and display the image------------------------------------------
function displayBackground(){
	var customBackground = JSON.parse(localStorage.getItem("background"));
	if(customBackground){		
		workspace.background.set("image("+customBackground+")");
	}
}

//--------------------------------save a canvas image as a png------------------------------------------
function canvasToPng(){
	if(selectedImage)
		unselect(selectedImage);
	var canvas = document.getElementById("album_canvas");
	var albumToSave = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
	window.location.href = albumToSave;
}
