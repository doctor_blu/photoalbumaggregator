var canvasMouseX = 0;
var canvasMouseY = 0;

$(document).ready(function() {
	var images = $("#tools_right .content img");
	for (i=0; i<images.length; i++) {
		images[i].setAttribute("draggable", "true");
		images[i].addEventListener("dragstart", handleDragStart, false);
		images[i].addEventListener("dragend", handleDragEnd, false);
	}
	images = $("#tools_left .content img");
	for (i=0; i<images.length; i++) {
		images[i].setAttribute("draggable", "true");
		images[i].addEventListener("dragstart", handleDragStart, false);
		images[i].addEventListener("dragend", handleDragEnd, false);
	}
});


function handleDragStart(e) {
	this.style.opacity = "0.4";
	e.dataTransfer.setData("text", e.target.getAttribute("src"));
	return true;	
}
function handleDragEnd(e) {
	this.style.opacity = "1";
	return true;
}



function handleDragOver(e) {
	$("#album_page").css("box-shadow", "2px 2px 10px #bbb");
	//console.log(workspace.mouse.x);
	return false;
}
function handleDragLeave(e) {
	$("#album_page").css("box-shadow", "2px 2px 5px #000");
	return false;
}



function handleDrop(e) {
	$("#album_page").css("box-shadow", "2px 2px 5px #000");
	var src = e.dataTransfer.getData("text");
	var x = e.pageX - $("canvas").offset().left;
	var y = e.pageY - $("canvas").offset().top;
	insertImage(src,x,y);
	return false;
}


function injectImgIntoCanvas(url) {
	insertImage(url);
}

