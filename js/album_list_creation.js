$(document).ready(function() {
	createList();
});

function createList () {
  var container = $("#wrapper");
  var keys = Object.keys(localStorage); // get all the keys in the array

  for (i=0; i<keys.length; i++) {
  	var album_id = keys[i];
  	if(!isNaN(parseInt(album_id))) { //check if the key is a number or a word (probabily in localStorage there are other data stored)
		var objectStored = JSON.parse(localStorage.getItem(album_id));
		var name = objectStored.title;
		var preview_src = "img/Utilities/trollface.png"; 
		var edit_link = "./edit.html?album_id="+album_id;
		
		var delete_link = "javascript:deleteAlbum("+album_id+")"; 
		
		//creating the article
		var article = document.createElement("article");
		article.setAttribute("class", "album");
		container.append($(article));
		
		//creating the header
		var header = document.createElement("header");
		var title = document.createElement("h1");
		var subtitle = document.createElement("p");
		
		$(subtitle).html(name);
		$(title).html("Album name:")
		$(header).append($(title));
		$(header).append($(subtitle));
		$(article).append($(header));
		
		//creating the delete link
		var del = document.createElement("a");
		del.setAttribute("href", delete_link);
		//del.onclick = deleteAlbum(album_id);
		$(del).html("Delete");
		
		//creating the edit link
		var modify = document.createElement("a");
		modify.setAttribute("href", edit_link);
		$(modify).html("Edit");
		
		var link_container = document.createElement("div");
		$(link_container).append($(modify));
		$(link_container).append($(del));
		$(article).append($(link_container));
		
		//creating the image
		var image = document.createElement("img");
		image.setAttribute("alt", "");
		image.setAttribute("src", objectStored.thumbnail);
		$(article).append($(image));
	}	
  }
  
  if (keys.length == 0) {
		var empty_mex = document.createElement("p");
		$(empty_mex).html("You don't have any saved album !! Create a <a href='./edit.html'>new album</a> now !")
		container.append($(empty_mex));
	}
	
};


//-------------------------------------------------------------DELETE ALBUM----------------------------------------------------------------------------------------------------
deleteAlbum = function(album_id){
	localStorage.removeItem(album_id);
	window.location.reload();
}
