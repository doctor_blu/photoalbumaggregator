$(document).ready(function() {
	setTimeout(100, resizeContentSection());
	$(window).resize(function() {
		resizeContentSection();
	});
});

function resizeContentSection () {
  var h = $(window).height();
  var w = $(window).width();
  var padding = 20;
  var f_width = 700;
  var min_height = 500;
  var min_width = 700;
  var header = parseInt($("header").height()) + parseInt($("header").css("margin-bottom").replace("px", ""));
  var footer = parseInt($("footer").height()) + parseInt($("footer").css("margin-top").replace("px", "")) + parseInt($("footer").css("padding-top").replace("px", ""));
  
  if (h>min_height) {
  	$("#content").height(h-header-footer-2*padding);
  	$("footer").css("position", "absolute");
  	$("footer").css("margin-left", "0")
  	if (w>min_width)
  		$("footer").css("left", (w-f_width)/2+"px");
  	else
  		$("footer").css("left", "0");
  } else {
  	$("#content").height(min_height-header-footer-2*padding);
  	$("footer").css("position", "static");
  	if (w>min_width)
  		$("footer").css("margin-left", (w-f_width)/2+"px")
  	else
  		$("footer").css("margin-left", "0");
  }
  
  $("footer").width(f_width); 
   
}
