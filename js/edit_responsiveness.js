$(document).ready(function() {
	$(window).resize(function() {
		resizeElements();
	});
	resizeElements();
});

function resizeElements() {
	//window dim
	var w = $(window).width();
	var h = $(window).height();
	//toolbars
	var t_b = $("#tools_bottom");
	t_b.height(45);
	var t_r = $("#tools_right");
	t_r.width(252);
	var t_l = $("#tools_left");
	t_l.width(150);
	//collapsed toolbars
	var c_b = $("#collapsed_bottom");
	var c_r = $("#collapsed_right");
	var c_l = $("#collapsed_left");
	//canvas container
	var canvas = $("#album_page");
	canvas.width(700);
	canvas.height(600);
	//header (navbar container)
	var header = $("header");
	//collapse button
	var cll_btn_r = $("#collapse_on_r");
	var cll_btn_l = $("#collapse_on_l");
	//thresholds
	var horizontal_limit = 800;
	var horizontal_collapse_limit = 1140;
	var vertical_limit = 725;
	
	//tools_bottom horizontal position
	if (w < horizontal_limit) {
		t_b.css("left", (horizontal_limit-canvas.width())/2+"px");
		canvas.css("left", (horizontal_limit-canvas.width())/2+"px");
	} else if (w < horizontal_collapse_limit) {
		t_b.css("left", (w -canvas.width())/2+"px");
		canvas.css("left", (w - canvas.width())/2+"px");
	} else {
		t_b.css("left", t_l.width() + (w - t_l.width() - t_r.width() -canvas.width())/2+"px");
		canvas.css("left", t_l.width() + (w - t_l.width() - t_r.width() -canvas.width())/2+"px");
	}
	
	//vertical position
	if (h > vertical_limit) {
		var remaining_vertical_space = h - header.height() - t_b.height() - canvas.height();
		t_l.css("top", header.height()+remaining_vertical_space/2+"px");
		t_r.css("top", header.height()+remaining_vertical_space/2+"px");
		canvas.css("top", header.height()+remaining_vertical_space/2+"px");
		c_l.css("top", header.height()+remaining_vertical_space/2+"px");
		c_r.css("top", header.height()+remaining_vertical_space/2+"px");
	} else {
		t_l.css("top", header.height()+10+"px");
		t_r.css("top", header.height()+10+"px");
		canvas.css("top", header.height()+10+"px");
		c_l.css("top", header.height()+10+"px");
		c_r.css("top", header.height()+10+"px");
	}
	
	//copy same settings to collapsed toolbars
	c_b.css("left", t_b.css("left"));
	
	
	//COLLAPSING CHECK
	if ($(window).width() > horizontal_collapse_limit) {
		t_r.css("display", "");
		c_r.css("display", "");
		t_l.css("display", "");
		c_l.css("display", "");
		cll_btn_r.hide();
		cll_btn_l.hide();
	} else {
		cll_btn_r.show();
		cll_btn_l.show();
	}
	
}

function collapseRight() {
	var t_r = $("#tools_right");
	var c_r = $("#collapsed_right");
	
	if ($(window).width() < 1140) {
		t_r.hide();
		c_r.show();
	}
}

function showRight() {
	var t_r = $("#tools_right");
	var c_r = $("#collapsed_right");
	
	if ($(window).width() < 1140) {
		c_r.hide();
		t_r.show();
	}
}

function collapseLeft() {
	var t_l = $("#tools_left");
	var c_l = $("#collapsed_left");
	
	if ($(window).width() < 1140) {
		t_l.hide();
		c_l.show();
	}
}

function showLeft() {
	var t_l = $("#tools_left");
	var c_l = $("#collapsed_left");
	
	if ($(window).width() < 1140) {
		c_l.hide();
		t_l.show();
	}
}

