var workspace;
var selectedImage = null;
var wasTransforming = false; //set during transforming for preventing object unselection
var tmpZIndex = 0; //For remembering which was an object's ZIndex before selection
var backCanvas = document.createElement('canvas');
//Configuration
var initialSize = 3;

//Handles declaration
var translationHandle;
var rotationHandle;

//Canvas initialization and object types definition (see .register())
oCanvas.domReady(function(){
	
	$("#effectsControls input, #effectsControls label").click(applyEffects);

	workspace = oCanvas.create({
		canvas: "#album_canvas"
	});
	//------------------------------------------------Handles
	translationHandle = workspace.display.image({
		x: 0,
		y: 0,
		width:40,
		height:40,
		origin: { x: "center", y: "center" },
		image: "img/Canvas/traslationHandle.png"
	})
	
	rotationHandle = workspace.display.image({
		x: 0,
		y: 0,
		width:20,
		height:20,
		rotation:-90,
		origin: { x: "center", y: "center" },
		image: "img/Canvas/rotationHandle.png"
	});
			
	scalingHandle = workspace.display.image({
		x: 0,
		y: 0,
		width:20,
		height:20,
		origin: { x: "center", y: "center" },
		image: "img/Canvas/scalingHandle.png"
	});
	
	shadingHandle = workspace.display.image({
		x: 0,
		y: 0,
		width:40,
		height:40,
		origin: { x: "center", y: "center" },
		image: "img/Canvas/shadingHandle.png"
	});
		
	
	workspace.bind("click",function(event){//Unselect object if clicking on empty region
		event.stopPropagation();
 		if(selectedImage!=null&&!wasTransforming) unselect(selectedImage);
 		wasTransforming = false;
 	});
	//image type definition
	workspace.display.register("userImage", {
					shapeType: "rectangular"
				}, function (ctx) {
						this.width = this.image.width;
						this.height = this.image.height;
						this.setOrigin("center","center");
						
						//Drawing border
						ctx.beginPath();

						if (this.strokeWidth > 0) {
							ctx.strokeStyle = this.strokeColor;
							ctx.lineWidth = this.strokeWidth*2;
							ctx.strokeRect(0-this.width/2, 0-this.height/2, this.width, this.height);
						}
						ctx.closePath();
						
						//Image drawing and effects applying
						//abs_x and abs_y are position coordinates (in this case at center of image)
						//getOrigin.x and .y are origin offset from top-left corner
						if(!(this.parent.invertEffect || this.parent.greyEffect || this.parent.noiseEffect|| this.parent.embossEffect|| this.parent.sepiaEffect)) ctx.drawImage(this.image, 0 - this.width / 2, 0 - this.height / 2, this.width, this.height);
						else{
							//-----------------------------------------------FX
							backCanvas.width = this.width;
							backCanvas.height = this.height;
							var backCtx = backCanvas.getContext("2d");
							backCtx.drawImage(this.image,0,0,this.width,this.height);
							//drawing image on in-memory canvas and applying transformation
							var imgd = backCtx.getImageData(0,0,this.width,this.height);
							var data = imgd.data;
							if(this.parent.noiseEffect){
								data = noiseEffect(data);
							}
							if(this.parent.invertEffect){
								data = invertEffect(data);
							}
							if(this.parent.greyEffect){
								data = greyEffect(data);
							}
							if(this.parent.sepiaEffect){
								data = sepiaEffect(data);
							}
							if(this.parent.embossEffect){
								data = embossEffect(this.image,imgd);
							}
							
							backCtx.putImageData(imgd, 0, 0);

							ctx.drawImage(backCanvas, 0 - this.width / 2, 0 - this.height / 2);
						}
				});
				
});

//------------------------------------------------------------Inserting
//Function for creating image and handles in oCanvas
function insertImage(file,x,y,callback){
	var image = new Image();
	image.onload = function(){
		//Image resizing so that it fits inside the canvas
		if(image.width>(workspace.width/initialSize)||image.height>(workspace.height/initialSize)) {
			var ratio = image.height/image.width;
			if (image.width == Math.max(image.height,image.width)){ //width is biggest size
				image.width = workspace.width/initialSize;
				image.height = image.width*ratio;
			}else { //Height is biggest size
				image.height = workspace.height/initialSize;
				image.width = image.height/ratio;
			}
		}
		
		//Create image and handles' container object (the one that is moved and rotated)
		imageContainer = workspace.display.rectangle({
			x:x,
			y:y,
			height:image.height,
			width:image.width,
			stroke:"0px #000000"
		});
		imageContainer.setOrigin("center","center");
		
		//Create oCanvas userImage object
		canvasImage = workspace.display.userImage({
			x: 0,
			y: 0,
			image: image
		});
		
		//Combine everything together: image and handles inside the container
		imageContainer.addChild(canvasImage);
		//Variables INITIALIZATION
		imageContainer.shadowX = -10;
		imageContainer.shadowY = -10;
		imageContainer.selected = false;
		imageContainer.bind("click",function(event){
		event.stopPropagation();
			select(this);
		});
		
		imageContainer.greyEffect = imageContainer.sepiaEffect = imageContainer.noiseEffect = imageContainer.embossEffect = imageContainer.invertEffect = false;
		
		workspace.addChild(imageContainer);
		console.log("Quando aggiungo al workspace: zIndex= "+imageContainer.zIndex+" tmpZIndex="+tmpZIndex);
		select(imageContainer);
		bringFront();
		console.log("Dopo il brinf to front: zIndex= "+imageContainer.zIndex+" tmpZIndex="+tmpZIndex);
		console.log("Dopo la selezione: zIndex= "+imageContainer.zIndex+" tmpZIndex="+tmpZIndex);
		workspace.redraw();
		if(callback!=undefined) callback();
	};
	image.src = file;
	
}


//Caption fields
function addCaption(content,color,font,callback){
	if(content==undefined) content = $("#captionField").val();
	if(color==undefined) color = $("#captionColorField div").css("backgroundColor");
	if(font==undefined){
		$("#captionFontSelection input").each(function(){
			if($(this).attr("checked")) font = "bold 30px "+$(this).val();
		});
	}
	if(content.length>0){
		var text = workspace.display.text({
		x: 0,
		y: 0,
		origin: { x: "center", y: "center" },
		font: font,
		text: content,
		fill: color
		});	
		
		imageContainer = workspace.display.rectangle({
			x:$("canvas").width()/2,
			y:$("canvas").height()/2,
			height:text.height,
			width:text.width,
			stroke:"0px #000000"
		});
		imageContainer.setOrigin("center","center");
		imageContainer.shadowX = -10;
		imageContainer.shadowY = -10;
		imageContainer.selected = false;
		imageContainer.bind("click",function(event){
			event.stopPropagation();
			select(this);
		});
		
		imageContainer.addChild(text);
		workspace.addChild(imageContainer);
		imageContainer.children[0].shadow = "0 0 0 transparent";
		select(imageContainer);
		bringFront();
		workspace.redraw();
		$("#captionField").val("");
		
	}
	if(callback!=undefined) callback();
}

//Controls
function newCaption(){
	controlsPopup('#captionControls',addCaption);
	var color = "#008888";
	$("#captionColorField div").css("backgroundColor",color);
    $('#captionColorField').ColorPicker({
    	color: color,
    	onShow: function(){$(".colorpicker").css("z-index","10000");},
        onChange: function(hsb,hex,rgb){$('#captionColorField div').css('backgroundColor', '#' + hex);}
    });
}
//------------------------------------------------------FX and object managing
//Image selection and deletion functions
function select(imageContainer){
	console.log("Nella selezione, all'inizio, zIndex: "+imageContainer.zIndex);
	if(!imageContainer.selected){
		if(selectedImage!=null) unselect(selectedImage);
		imageContainer.selected = true;
		imageContainer.stroke = (10+imageContainer.children[0].strokeWidth*2)+"px #8ff";
		selectedImage = imageContainer;
		tmpZIndex = selectedImage.zIndex;
		console.log("Nella selezione, verso la fine, zIndex: "+selectedImage.zIndex+" tmpZIndex: "+tmpZIndex);
		selectedImage.zIndex = "front";
		workspace.redraw();
	}
}
function unselect(imageContainer){
	imageContainer.selected = false;
	imageContainer.stroke = "0px transparent"
	//All editings have to be interrupted here
	stopEditings();
	selectedImage.zIndex = tmpZIndex;
	selectedImage = null;
	console.log("Dopo la deselezione, l'imagine che era selezionata: zIndex= "+imageContainer.zIndex+" tmpZIndex="+tmpZIndex);
	workspace.redraw();
}

function erase(){
	workspace.removeChild(selectedImage);
}
//Z-index functions
function sendBack(){
	selectedImage.zIndex = "back";
	tmpZIndex = selectedImage.zIndex;//For preventing the object to be restored to its previous index when unselected
	workspace.redraw();
}

function bringFront(){
	selectedImage.zIndex = "front";
	tmpZIndex = selectedImage.zIndex;//For preventing the object to be restored to its previous index when unselected
	workspace.redraw();
}

//Selected object borders
function defineBorders(){
	if(selectedImage!=null){
		controlsPopup('#bordersControls',setBorders);
		var color;
		if(selectedImage.children[0].strokeColor==undefined||selectedImage.children[0].strokeColor=="transparent")  color = "#005555";
		else color = selectedImage.children[0].strokeColor;
		
		if(selectedImage.children[0].strokeWidth==0)  $("#bordersControls #bordersWidthField").val("4");
		else  $("#bordersControls #bordersWidthField").val(selectedImage.children[0].strokeWidth/2);
		
		$("#bordersColorField div").css("backgroundColor",color);
	    $('#bordersColorField').ColorPicker({
	    	color: color,
	    	onShow: function(){$(".colorpicker").css("z-index","10000");},
	        onChange: function(hsb,hex,rgb){$('#bordersColorField div').css('backgroundColor', '#' + hex);}
	    });
    }
}

function setBorders(width,color){
	if(width==undefined) width = $("#bordersWidthField").val();
	if(isNaN(width)) width = 6;
	if(color==undefined) color = $("#bordersColorField div").css("backgroundColor");
	
	selectedImage.children[0].stroke = (width*2)+"px "+color;
	selectedImage.stroke = (10+selectedImage.children[0].strokeWidth*2)+"px #8ff";
	workspace.redraw();
}

//Selected object translation
function move(){
	stopEditings();
	selectedImage.dragAndDrop(true);
	selectedImage.addChild(translationHandle);
	workspace.redraw();
}

//Selected object Rotation
function rotate(){
	stopEditings();
	selectedImage.addChild(rotationHandle);
	rotationHandle.x = selectedImage.width/2+5;
	rotationHandle.y = 0-selectedImage.height/2-5;
	var handleOffset = rotationHandle.x/(Math.sqrt(Math.pow(rotationHandle.x,2)+Math.pow(rotationHandle.y,2)));
	handleOffset = Math.acos(handleOffset)*180/Math.PI;
	rotationHandle.unbind("mousedown").bind("mousedown",function(event){
		var startX, startY;
		workspace.bind("mousemove",function(){
			wasTransforming = true;
			startX = workspace.mouse.x-selectedImage.abs_x;
			startY = workspace.mouse.y-selectedImage.abs_y;
			theta = Math.atan2(startY,startX)*180/Math.PI;
			selectedImage.rotateTo(theta+handleOffset);
			workspace.redraw();
		});
		workspace.bind("mouseup",function(){
			workspace.unbind("mousemove mouseup");
		});
	});
	workspace.redraw();
}

//Selected object scaling
function scale(){

	var testline;//To delete
// testLineX = workspace.display.line({
// 			start: { x:0,y:0},
//  			end: { x:0,y:0 },
//  			stroke: "20px #f00",
//  			cap: "round",
//  		});
// testLineY = workspace.display.line({
// 			start: { x:0,y:0},
//  			end: { x:0,y:0 },
//  			stroke: "20px #00f",
//  			cap: "round"
//  		});
		
	stopEditings();
	selectedImage.addChild(scalingHandle);
	function placeHandle(){
		scalingHandle.x = selectedImage.width/2+5;
		scalingHandle.y = selectedImage.height/2+5;
	}
	placeHandle();
	var counter = 0;
	scalingHandle.unbind("mousedown").bind("mousedown",function(event){
		event.stopPropagation();
		var startX, startY, nowX, nowY, cornerX, cornerY, scaleX=1, scaleY=1;
		cornerX = selectedImage.abs_x-rotatePointer(selectedImage.getOrigin().x,selectedImage.getOrigin().y)[0];
		cornerY = selectedImage.abs_y-rotatePointer(selectedImage.getOrigin().x,selectedImage.getOrigin().y)[1];
		startX = workspace.mouse.x-cornerX;
		startY = workspace.mouse.y-cornerY;
		nowX = startX, nowY = startY;
		wasTransforming = true;
		
		//workspace.addChild(testLineX);
		//workspace.addChild(testLineY);
		workspace.redraw();
		//field1.value = "cornerX: "+cornerX.toFixed(2)+" cornerY: "+cornerY.toFixed(2);
		workspace.bind("mousemove",function(event){
			event.stopPropagation();
			nowX = workspace.mouse.x-cornerX;
			nowY = workspace.mouse.y-cornerY;
// 			
// 			testLineX.start =  { x:200,y:200}, testLineX.end =  { x:200-(nowX/startX)*100,y:200 };
// 			testLineY.start =  { x:200,y:200}, testLineY.end =  { x:200,y:200-(nowY/startY)*100 };
// 			
			scaleX = Math.sqrt(Math.pow(nowX,2)+Math.pow(nowY,2))/Math.sqrt(Math.pow(startX,2)+Math.pow(startY,2));
			scaleY = Math.sqrt(Math.pow(nowX,2)+Math.pow(nowY,2))/Math.sqrt(Math.pow(startX,2)+Math.pow(startY,2));
			//field2.value = "nowX/startX: "+nowX.toFixed(2)+"/"+startX.toFixed(2)+" nowY/startY: "+nowY.toFixed(2)+"/"+startY.toFixed(2)+" ScaleX:"+scaleX.toFixed(2)+" ScaleY:"+scaleY.toFixed(2);
			if(counter==5) {
				selectedImage.scale(Math.abs(scaleX),Math.abs(scaleY));
				workspace.redraw();
				counter = 0;
			}
			counter++;
		});
		
		workspace.bind("mouseup",function(event){
			event.stopPropagation();
			if(selectedImage.children[0].type!="text"){
				selectedImage.scale(1,1);
				selectedImage.children[0].image.width = selectedImage.children[0].image.width*Math.abs(scaleX);
				selectedImage.children[0].image.height = selectedImage.children[0].image.height*Math.abs(scaleY);
				selectedImage.width = selectedImage.children[0].image.width;
				selectedImage.height = selectedImage.children[0].image.height;
				placeHandle();
			}else{
				selectedImage.children[1].remove();
				selectedImage.scale(1,1);
				selectedImage.children[0].size = selectedImage.children[0].size*Math.abs(scaleX);
				selectedImage.height = selectedImage.height*Math.abs(scaleX);
				selectedImage.width = selectedImage.width*Math.abs(scaleY);
				selectedImage.addChild(scalingHandle);
				placeHandle();
			}
			workspace.unbind("mousemove mouseup");
			workspace.redraw();
		});
	});
	workspace.redraw();
}

function restoreImageScaling(){
	selectedImage.children[0].image.width = selectedImage.width;
	selectedImage.children[0].image.height = selectedImage.height;
	workspace.redraw();
}


//Selected object shading
function shade(){
	stopEditings();
	var nowX=0, nowY=0;
	selectedImage.addChild(shadingHandle);
	shadingHandle.x = selectedImage.shadowX;
	shadingHandle.y = selectedImage.shadowY;
	function setShadow(){
		nowX = shadingHandle.x;
		nowY = shadingHandle.y;
		selectedImage.children[0].shadowColor = "#000";
		selectedImage.children[0].shadowOffsetX = rotatePointer(Math.min(Math.abs(nowX),50)*sign(-nowX),Math.min(Math.abs(nowY),50)*sign(-nowY))[0];
		selectedImage.children[0].shadowOffsetY = rotatePointer(Math.min(Math.abs(nowX),50)*sign(-nowX),Math.min(Math.abs(nowY),50)*sign(-nowY))[1];
		selectedImage.children[0].shadowBlur = Math.sqrt(Math.pow(nowX,2)+Math.pow(nowY,2))+10;
		selectedImage.shadowX = nowX;
		selectedImage.shadowY = nowY;
	}
	setShadow();
	workspace.redraw();
	shadingHandle.dragAndDrop({
		move:function(){
			setShadow();
			//field1.value = selectedImage.children[0].shadowOffsetX;
		},
	});
}

//Stop editings on selected object
function stopEditings(){
	if(selectedImage!=null){
		if(selectedImage.children.indexOf(translationHandle)>-1){ //Stop translation
			selectedImage.removeChild(translationHandle);
			selectedImage.dragAndDrop(false);
		}
		if(selectedImage.children.indexOf(rotationHandle)>-1){ //Stop translation
			selectedImage.removeChild(rotationHandle);
		}
		if(selectedImage.children.indexOf(scalingHandle)>-1){ //Stop scaling
			selectedImage.removeChild(scalingHandle);
		}
		if(selectedImage.children.indexOf(shadingHandle)>-1){
			selectedImage.removeChild(shadingHandle);
			shadingHandle.dragAndDrop(false);
			shadingHandle.x = 0,shadingHandle.y = 0;
		}
	}
}


//Utilities
function sign(number){
	if(number>=0) return 1;
	else return -1;
}

function rotatePointer(x,y,sign){
	tranX = x;
	tranY = y;
	if (sign==undefined) sign = 1;
	angle = selectedImage.rotation/180*Math.PI;
	newX = tranX*Math.cos(angle)-tranY*Math.sin(angle);
	newY = tranX*Math.sin(angle)+tranY*Math.cos(angle);
	return [newX,newY];
}

//-------------------------------------------------------FX
function effects(){
	if(selectedImage.children[0].type!="text"){
		if(selectedImage.greyEffect==true) $("#effectsControls #greyEffect").attr("checked","");
		if(selectedImage.noiseEffect==true) $("#effectsControls #noiseEffect").attr("checked","");
		if(selectedImage.invertEffect==true) $("#effectsControls #invertEffect").attr("checked","");
		if(selectedImage.sepiaEffect==true) $("#effectsControls #sepiaEffect").attr("checked","");
		if(selectedImage.embossEffect==true) $("#effectsControls #embossEffect").attr("checked","");
		controlsPopup("#effectsControls",function(){
			applyEffects();
			$("#effectsControls input").removeAttr("checked");
		});
	}
}

function applyEffects(){
	$("#effectsControls input").trigger('blur');
	setGreyEffect($("#effectsControls #greyEffect").attr("checked")!=undefined);
	setNoiseEffect($("#effectsControls #noiseEffect").attr("checked")!=undefined);
	setInvertEffect($("#effectsControls #invertEffect").attr("checked")!=undefined);
	setEmbossEffect($("#effectsControls #embossEffect").attr("checked")!=undefined);
	setSepiaEffect($("#effectsControls #sepiaEffect").attr("checked")!=undefined);
	workspace.redraw();
	
}

function setGreyEffect(value){
	selectedImage.greyEffect=value;
}
function setNoiseEffect(value){
	selectedImage.noiseEffect=value;
}
function setInvertEffect(value){
	selectedImage.invertEffect=value;
}
function setEmbossEffect(value){
	selectedImage.embossEffect=value;
}
function setSepiaEffect(value){
	selectedImage.sepiaEffect=value;
}

//ContentSelector = css selector; callback = function executed on closing.
function controlsPopup(contentSelector,callback){
	var positioning = computePopupPositioning();
	$(contentSelector).bPopup({
            onClose: callback,
            position: positioning,
            fadeSpeed: 'slow', //can be a string ('slow'/'fast') or int
            followSpeed: 1500, //can be a string ('slow'/'fast') or int
            modalColor: 'none',
            follow: [true,true]
        });
}

function computePopupPositioning(){
	var x = $("canvas").offset().left;
	var y = $("canvas").offset().top;
	return [x,y];
}
